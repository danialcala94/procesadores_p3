// Author: ALCAL� VALERA, DANIEL

package myjflex;
//Area de codigo, importaciones y paquetes

import java.io.*;
%%
//Area de opciones y declaraciones
%class Lexer
%int
%standalone
%line
%column
%public

%{
//Preparo un objeto String para almacenar el contenido de las cadenas de texto
String tempCadena = new String("");
%}
%state STATE_JAVA
%state STATE_JAVA_STRING
%state STATE_PHP
%state STATE_PHP_STRING


Finlinea = \n|\r|\r\n
Blanco = {Finlinea}|[ \t\f]

Variable = [a-zA-Z]+[a-zA-Z0-9_]*
PHP_VARIABLE = "$"[a-zA-Z]+[a-zA-Z0-9_]*
Punto = "."
FraseAux = [a-zA-Z0-9 .,]+
Asignacion = [=]
CadIni = \"
CadFin = \"
FinLineaCodigo = ";"

%%
//Area de reglas y acciones

<YYINITIAL> {
	{Blanco} { System.out.print(""); }
	"BeginJava" { System.out.println("JAVA_INI_STATE"); yybegin(STATE_JAVA); }
	"BeginPHP" { System.out.println("PHP_INI_STATE"); yybegin(STATE_PHP); }
}
<STATE_JAVA> {
	{Blanco} { System.out.print(""); }
	"EndJava" { System.out.println("JAVA_FIN_STATE"); System.out.println(); yybegin(YYINITIAL); }
	"(" { System.out.println("JAVA_OPENBRACKETS"); }
	")" { System.out.println("JAVA_CLOSEBRACKETS"); }
	{Variable} { System.out.println("JAVA_ID(" + yytext() + ")"); }
	{Punto} { System.out.println("JAVA_PUNTO"); }
	{Asignacion} { System.out.println("JAVA_ASIGNATION"); }
	{FinLineaCodigo} { System.out.println("JAVA_FIN_CODELINE"); }
	{CadIni} { System.out.println("JAVA_CAD_INI"); yybegin(STATE_JAVA_STRING); }
	
} 
<STATE_JAVA_STRING> {
	{Blanco} { System.out.println("Error in line number " + (yyline + 1) + " column " + (yycolumn + 1) + ".\n     ERROR: JAVA_breakline_when_string_declaration_error."); tempCadena = ""; }
	{FraseAux} { System.out.println("JAVA_STRING"); }
	{CadFin} { System.out.println("JAVA_CAD_FIN"); yybegin(STATE_JAVA); }
}
<STATE_PHP> {
	"$$".* { System.out.println("Error in line number " + (yyline + 1) + ".\n     ERROR: PHP_variable_declaration_error."); tempCadena = ""; }
	{Blanco} { System.out.print(""); }
	"EndPHP" { System.out.println("PHP_FIN_STATE"); yybegin(YYINITIAL); }
	"(" { System.out.println("PHP_OPENBRACKETS"); }
	")" { System.out.println("PHP_CLOSEBRACKETS"); }
	{FinLineaCodigo} { System.out.println("PHP_FIN_CODELINE"); }
	{PHP_VARIABLE} { System.out.println("PHP_ID(" + yytext().substring(1) + ")"); }
	{Asignacion} { System.out.println("PHP_ASIGNATION"); }
	{CadIni} { System.out.println("PHP_CAD_INI"); yybegin(STATE_PHP_STRING); }
	{Variable} { System.out.println("PHP_" + yytext() + "_FUNCTION"); }
	
}
<STATE_PHP_STRING> {
	{Blanco} { System.out.println("Error in line number " + (yyline + 1) + " column " + (yycolumn + 1) + ".\n     ERROR: PHP_breakline_when_string_declaration_error."); tempCadena = ""; }
	{FraseAux} { System.out.println("PHP_STRING"); }
	{CadFin} { System.out.println("PHP_CAD_FIN"); yybegin(STATE_PHP); }
}


 

