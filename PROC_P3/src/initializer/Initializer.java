/*
 * Author: ALCALA VALERA, DANIEL
 * PRACTICE: P3 JFlex con estados
 */
package initializer;

import practices.PracticeThree;

public class Initializer {

	public static void main(String[] args) {
		PracticeThree.execute();
	}
	
}
