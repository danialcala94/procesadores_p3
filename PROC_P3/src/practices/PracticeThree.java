/*
 * Author: ALCALA VALERA, DANIEL
 * PRACTICE: P3 JFlex con estados
 */
package practices;

import java.io.File;

import myjflex.Lexer;

public class PracticeThree {

	private static final String OUTPUT_DIR = "src/myjflex/";
	private static final String FILES_DIR = "files/";
	private static final String EXERCISE_ONE = FILES_DIR + "Exercise.jflex";
	private static final String EXAMPLE_ONE = FILES_DIR + "exOne.txt";
	
	/**
	 * Generates the five analyzers and analyze the file with them.
	 */
	public static void execute() {
		String paths[] = {EXERCISE_ONE};
		generateAllAnalyzers(paths, OUTPUT_DIR);
		
		if (new File(OUTPUT_DIR + "Lexer.java").exists()) {
			String arguments[][] = {{EXAMPLE_ONE}};
			useAnalyzer(arguments);
		}
	}
	
	/**
	 * Deletes the previous analyzers if existing.
	 * @return
	 */
	private static boolean deletePreviousAnalyzers() {
		int cont = 0;
		
		if (new File(OUTPUT_DIR + "Lexer.java").delete()) {
			cont++;
		}

		System.out.println("Deleted " + cont + " analyzers.");
		
		if (cont == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * It creates an analyzer using JFlex.
	 * @param path
	 * @param outputDir
	 */
	private static void generate(String path, String outputDir) {
		String args[] = {path, "-d", outputDir};
		jflex.Main.main(args);
	}
	
	/**
	 * Generates the five analyzers for the first practice.
	 * @param path
	 * @param outputDir
	 */
	private static void generateAllAnalyzers(String[]path, String outputDir) {
		deletePreviousAnalyzers();
		
		for (int i = 0; i < path.length; i++) {
			System.out.println("Generating analyzer number " + (i + 1));
			generate(path[i], outputDir);
		}
	}

	
	/**
	 * Analyze the file with the five analyzers generated.
	 * @param fileToAnalyze
	 */
	private static void useAnalyzer(String[][] fileToAnalyze) {
		System.out.println("\nExecuting Analyzer: ");
		Lexer.main(fileToAnalyze[0]);
	}
}
